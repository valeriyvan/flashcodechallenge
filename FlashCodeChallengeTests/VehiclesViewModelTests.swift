import XCTest
import Result
@testable import FlashCodeChallenge

class VehiclesViewModelTests: XCTestCase {

    private var viewModel : VehiclesViewModel!

    private var viewModelDelegate: VehiclesViewModelDelegateMock!
    private var api : VehiclesApiMock!

    override func setUp() {
        super.setUp()
        self.api = VehiclesApiMock()
        self.viewModelDelegate = VehiclesViewModelDelegateMock()
        self.viewModel = VehiclesViewModel(delegate: self.viewModelDelegate, apiClient: self.api)
    }

    override func tearDown() {
        self.viewModel = nil
        self.viewModelDelegate = nil
        self.api = nil
        super.tearDown()
    }

    func testFailWithNoVehicles() {
        let expectation = XCTestExpectation(description: "Fail with no vehicles")

        api.vehicles = nil

        viewModelDelegate.successCompletion = { _ in
            XCTAssert(false, "ViewModel should not succeed when there's no vehicles array")
        }

        viewModelDelegate.failCompletion = { _ in
            expectation.fulfill()
        }

        viewModel.fetch()

        wait(for: [expectation], timeout: 0.1)
    }

    func testSuccessWithEmptyArray() {
        let expectation = XCTestExpectation(description: "Success with empty array")

        api.vehicles = []

        viewModelDelegate.successCompletion = { _ in
            expectation.fulfill()
        }

        viewModelDelegate.failCompletion = { _ in
            XCTAssert(false, "ViewModel should not fail when api provides empty vehicles array")
        }

        viewModel.fetch()

        wait(for: [expectation], timeout: 0.1)
    }

    func testSuccessWithSingleVehicle() {
        let expectation = XCTestExpectation(description: "Success with single vehicle")

        api.vehicles = [Vehicle(id: 0, latitude: 0.0, longitude: 0.0)]

        viewModelDelegate.successCompletion = { vehicles in
            XCTAssertTrue(vehicles.count == 1)
            expectation.fulfill()
        }

        viewModelDelegate.failCompletion = { _ in
            XCTAssert(false, "ViewModel should not fail when api provides array with single vehicle")
        }

        viewModel.fetch()

        wait(for: [expectation], timeout: 0.1)
    }

    func testSuccessWithThreeVehicles() {
        let expectation = XCTestExpectation(description: "Success with three vehicles in array")

        api.vehicles = [Vehicle(id: 0, latitude: 0.0, longitude: 0.0),
                        Vehicle(id: 1, latitude: 1.0, longitude: 1.0),
                        Vehicle(id: 2, latitude: 2.0, longitude: 2.0)]

        viewModelDelegate.successCompletion = { vehicles in
            XCTAssertTrue(vehicles.count == 3)
            expectation.fulfill()
        }

        viewModelDelegate.failCompletion = { _ in
            XCTAssert(false, "ViewModel should not fail when api provides empty vehicles list")
        }

        viewModel.fetch()

        wait(for: [expectation], timeout: 0.1)
    }

}

private final class VehiclesApiMock: VehiclesApiProtocol {
    var vehicles: [Vehicle]?

    func fetchVehicles(parametriser: ((URL) -> URL)?, queue: DispatchQueue,
                       completion: @escaping (Result<[Vehicle], ApiError>) -> Void)
    {
        if let vehicles = vehicles {
            queue.async { completion(.success(vehicles)) }
        } else {
            queue.async { completion(.failure(.badResponse)) }
        }
    }

    func cancel() {
        vehicles = nil
    }
}

private final class VehiclesViewModelDelegateMock: VehiclesViewModelDelegate {
    var successCompletion: ((_: [Vehicle]) -> Void)?
    var failCompletion: ((_: String) -> Void)?

    func fetchCompleted(with vehicles: [Vehicle]) {
        successCompletion?(vehicles)
    }

    func fetchFailed(with reason: String) {
        failCompletion?(reason)
    }
}
