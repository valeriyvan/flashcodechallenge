import XCTest
import Result
@testable import FlashCodeChallenge

class VehicleDetailsViewModelTests: XCTestCase {

    private var viewModel : VehicleDetailsViewModel!

    private var viewModelDelegate: VehicleDetailsViewModelDelegateMock!
    private var api : VehicleDetailsApiMock!

    override func setUp() {
        super.setUp()
        self.api = VehicleDetailsApiMock()
        self.viewModelDelegate = VehicleDetailsViewModelDelegateMock()
        self.viewModel = VehicleDetailsViewModel(delegate: self.viewModelDelegate, apiClient: self.api)
    }

    override func tearDown() {
        self.viewModel = nil
        self.viewModelDelegate = nil
        self.api = nil
        super.tearDown()
    }

    func testFailWithNoVehicle() {
        let expectation = XCTestExpectation(description: "Fail with no vehicle")

        api.details = nil

        viewModelDelegate.successCompletion = { _ in
            XCTAssert(false, "ViewModel should not succeed when there's no vehicles array")
        }

        viewModelDelegate.failCompletion = { _ in
            expectation.fulfill()
        }

        viewModel.fetch(for: 0)

        wait(for: [expectation], timeout: 0.1)
    }

    func testSuccess() {
        let expectation = XCTestExpectation(description: "Success with vehicle")

        api.details = VehicleDetails(id: 0, name: "0", description: "0", latitude: 0.0, longitude: 0.0, batteryLevel: 0, timestamp: Date(), price: 0, priceTime: 0, currency: "¥")

        viewModelDelegate.successCompletion = { _ in
            expectation.fulfill()
        }

        viewModelDelegate.failCompletion = { _ in
            XCTAssert(false, "ViewModel should not fail when api provides vehicle details")
        }

        viewModel.fetch(for: 0)

        wait(for: [expectation], timeout: 0.1)
    }
    
}

private final class VehicleDetailsApiMock: VehicleDetailsApiProtocol {
    var details: VehicleDetails?

    func fetchVehicleDetails(parametriser: ((URL) -> URL)?, queue: DispatchQueue,
                             completion: @escaping (Result<VehicleDetails, ApiError>) -> Void)
    {
        if let details = details {
            queue.async { completion(.success(details)) }
        } else {
            queue.async { completion(.failure(.badResponse)) }
        }
    }

    func cancel() {
        details = nil
    }
}

private final class VehicleDetailsViewModelDelegateMock: VehicleDetailsViewModelDelegate {
    var successCompletion: ((_: VehicleDetails) -> Void)?
    var failCompletion: ((_: String) -> Void)?

    func fetchCompleted(with details: VehicleDetails) {
        successCompletion?(details)
    }

    func fetchFailed(with reason: String) {
        failCompletion?(reason)
    }
}
