import Foundation

final class Networking<T: Decodable> {

    func fetch(
        session: URLSession,
        url: URL,
        queue: DispatchQueue = .main,
        completion: @escaping (Result<T, ApiError>) -> Void) -> URLSessionTask
    {
        let task = session.dataTask(with: url) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                queue.async { completion(Result.failure(.badResponse)) }
                return
            }
            let statusCode = httpResponse.statusCode
            guard 200...299 ~= statusCode else {
                let localizedString = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                queue.async { completion(Result.failure(.failedResponse(statusCode: statusCode, localizedString: localizedString))) }
                return
            }
            guard let data = data else {
                queue.async { completion(Result.failure(.badResponse)) }
                return
            }
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            guard let output = try? decoder.decode(T.self, from: data) else {
                queue.async { completion(Result.failure(.decoding)) }
                return
            }
            queue.async { completion(Result.success(output)) }
        }
        return task
    }

}
