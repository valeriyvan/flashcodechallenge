import UIKit

class VehicleDetailsCoordinator: Coordinator {
    private weak var presenter: UIViewController?
    private let cardTransitioningDelegate: CardTransitioningDelegate
    private weak var mapViewController: MapViewController?
    private weak var vehicleDetailsViewController: VehicleDetailsViewController?
    private var vehicleId: Int
    private var vehicleDetailsViewModel: VehicleDetailsViewModel?

    init(presenter: UIViewController, mapViewController: MapViewController, vehicleId: Int) {
        self.presenter = presenter
        self.cardTransitioningDelegate = CardTransitioningDelegate()
        self.mapViewController = mapViewController
        self.vehicleId = vehicleId
    }

    func start() {
        let vehicleDetailsViewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "VehicleDetailsViewController") as! VehicleDetailsViewController
        vehicleDetailsViewController.vehicleDetailsViewControllerDelegate = self
        vehicleDetailsViewController.modalPresentationStyle = .custom
        vehicleDetailsViewController.transitioningDelegate = cardTransitioningDelegate
        let vehicleDetailsViewModel = VehicleDetailsViewModel(delegate: vehicleDetailsViewController)
        vehicleDetailsViewModel.fetch(for: vehicleId)
        presenter?.present(vehicleDetailsViewController, animated: true, completion: nil)
        self.vehicleDetailsViewController = vehicleDetailsViewController
        self.vehicleDetailsViewModel = vehicleDetailsViewModel
    }

    func dismiss() {
        mapViewController?.dismiss(animated: true, completion: nil)
    }

}

extension VehicleDetailsCoordinator: VehicleDetailsViewControllerDelegate {
    func dismissed(vehicleDetailsViewController: VehicleDetailsViewController) {
        mapViewController?.deselectAllAnnotations()
    }

    func rent(vehiclePresented VehicleDetailsViewController: VehicleDetailsViewController) {
        print("\(#function)") // TODO: !!!
    }
}
