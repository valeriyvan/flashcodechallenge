import UIKit

class MapCoordinator: Coordinator {
    private weak var window: UIWindow?
    private var vehiclesViewModel: VehiclesViewModel?
    private weak var mapViewController: MapViewController?
    private var vehicleDetailsCoordinator: VehicleDetailsCoordinator?

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let mapViewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mapViewController.mapViewControllerDelegate = self
        window?.rootViewController = mapViewController
        self.mapViewController = mapViewController
        let vehiclesViewModel = VehiclesViewModel(delegate: mapViewController)
        vehiclesViewModel.fetch()
        self.vehiclesViewModel = vehiclesViewModel
    }

    func dismiss() {}
}

extension MapCoordinator: MapViewControllerDelegate {
    func mapViewControllerDidDeselect() {
        vehicleDetailsCoordinator?.dismiss()
    }
    
    func mapViewControllerDidSelectVehicle(id: Int) {
        guard let mapViewController = mapViewController else { return }
        // Presenter could be something like navigation controller. Here the map controller is presenter itself.
        // But we keep both parameters to make initialiser more general.
        let vehicleDetailsCoordinator = VehicleDetailsCoordinator(presenter: mapViewController,
                                                                  mapViewController: mapViewController, vehicleId: id)
        vehicleDetailsCoordinator.start()
        self.vehicleDetailsCoordinator = vehicleDetailsCoordinator
    }
}
