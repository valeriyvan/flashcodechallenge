import Foundation

enum ApiError: Error {
    case server(originalError: Error)
    case badResponse
    case failedResponse(statusCode: Int, localizedString: String)
    case decoding
}
