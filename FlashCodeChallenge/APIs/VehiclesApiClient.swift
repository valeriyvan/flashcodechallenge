import Foundation

protocol VehiclesApiClientDelegate {
    func succeeded()
    func failed()
}

protocol VehiclesApiProtocol {
    func fetchVehicles(
        parametriser: ((URL) -> URL)?,
        queue: DispatchQueue,
        completion: @escaping (Result<[Vehicle], ApiError>) -> Void)

    func cancel()
}

final class VehiclesApiClient: VehiclesApiProtocol {

    private lazy var baseURL: URL = {
        return URL(string: "https://my-json-server.typicode.com/FlashScooters/Challenge/vehicles")!
    }()

    private let session: URLSession

    init(session: URLSession = .shared) {
        self.session = session
    }

    private var networking = Networking<[Vehicle]>()

    private weak var runningTask: URLSessionTask?

    func fetchVehicles(
        parametriser: ((URL) -> URL)? = nil,
        queue: DispatchQueue = .main,
        completion: @escaping (Result<[Vehicle], ApiError>) -> Void)
    {
        let url = parametriser?(baseURL) ?? baseURL
        let task = networking.fetch(session: session, url: url, queue: queue, completion: completion)
        runningTask = task
        task.resume()
    }

    func cancel() {
        runningTask?.cancel()
        runningTask = nil
    }
    
}
