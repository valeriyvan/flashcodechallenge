import Foundation

protocol VehicleDetailsApiClientDelegate {
    func succeeded()
    func failed()
}

protocol VehicleDetailsApiProtocol {
    func fetchVehicleDetails(
        parametriser: ((URL) -> URL)?,
        queue: DispatchQueue,
        completion: @escaping (Result<VehicleDetails, ApiError>) -> Void)

    func cancel()
}

final class VehicleDetailsApiClient: VehicleDetailsApiProtocol {
    private lazy var baseURL: URL = {
        return URL(string: "https://my-json-server.typicode.com/FlashScooters/Challenge/vehicles")!
    }()

    private let session: URLSession

    init(session: URLSession = .shared) {
        self.session = session
    }

    private var networking = Networking<VehicleDetails>()

    private weak var runningTask: URLSessionTask?

    func fetchVehicleDetails(
        parametriser: ((URL) -> URL)?,
        queue: DispatchQueue = .main,
        completion: @escaping (Result<VehicleDetails, ApiError>) -> Void)
    {
        let url = parametriser?(baseURL) ?? baseURL
        let task = networking.fetch(session: session, url: url, queue: queue, completion: completion)
        runningTask = task
        task.resume()
    }

    func cancel() {
        runningTask?.cancel()
        runningTask = nil
    }

}
