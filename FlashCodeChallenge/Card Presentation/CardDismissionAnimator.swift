import UIKit

class CardDismissionAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let containerView = transitionContext.containerView
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
        animations: {
            fromViewController.view.transform = CGAffineTransform(translationX: 0.0, y: containerView.bounds.height)
        },
        completion: { finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
