import UIKit

final class CardPresentationController: UIPresentationController {
    var touchForwardingView: TouchForwardingView!

    override var frameOfPresentedViewInContainerView: CGRect {
        let height: CGFloat = 200.0
        return CGRect(x: 0.0, y: containerView!.bounds.height - height, width: containerView!.bounds.width, height: height)
    }

    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        touchForwardingView = TouchForwardingView(frame: containerView!.bounds)
        touchForwardingView.passthroughViews = [presentingViewController.view];
        containerView?.insertSubview(touchForwardingView, at: 0)
    }
}
