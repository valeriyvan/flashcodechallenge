import UIKit
import MapKit

private let initialRegion = MKCoordinateRegion(
    center: CLLocationCoordinate2D(latitude: 52.5200, longitude: 13.4050),
    latitudinalMeters: 10_000.0,
    longitudinalMeters: 10_000.0)

protocol MapViewControllerDelegate: class {
    func mapViewControllerDidSelectVehicle(id: Int)
    func mapViewControllerDidDeselect()
}

final class MapViewController: UIViewController {

    weak var mapViewControllerDelegate: MapViewControllerDelegate?

    @IBOutlet private weak var mapView: MKMapView!

    // Actually we don't need this to be stored propery.
    // Real app probably will need this, so keep it stored property.
    private var vehicleAnnotations = [VehicleAnnotation]() {
        didSet {
            mapView.removeAnnotations(mapView.annotations)
            mapView.addAnnotations(vehicleAnnotations)
        }
    }

    let cardTransitioningDelegate = CardTransitioningDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.region = initialRegion
    }

    func deselectAllAnnotations() {
        mapView.selectedAnnotations.forEach { mapView.deselectAnnotation($0, animated: true) }
    }
}

extension MapViewController: VehiclesViewModelDelegate {

    func fetchCompleted(with vehicles: [Vehicle]) {
        self.vehicleAnnotations = vehicles.map(VehicleAnnotation.init)
    }

    func fetchFailed(with reason: String) {
        print("\(#function) \(reason)") // TODO: !!!
    }

}

extension MapViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, didSelect annotationView: MKAnnotationView) {
        guard let vehicleAnnotation = annotationView.annotation as? VehicleAnnotation else {
            return
        }
        mapViewControllerDelegate?.mapViewControllerDidSelectVehicle(id: vehicleAnnotation.id)
    }

    func mapView(_ mapView: MKMapView, didDeselect annotationView: MKAnnotationView) {
        guard annotationView.annotation is VehicleAnnotation else { return }
        mapViewControllerDelegate?.mapViewControllerDidDeselect()
    }

}
