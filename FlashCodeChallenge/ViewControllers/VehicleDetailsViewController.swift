import UIKit

protocol VehicleDetailsViewControllerDelegate: class {
    func dismissed(vehicleDetailsViewController: VehicleDetailsViewController)
    func rent(vehiclePresented VehicleDetailsViewController: VehicleDetailsViewController)
}

class VehicleDetailsViewController: UIViewController {

    @IBOutlet private weak var vehicleCardView: VehicleCardView!

    private var vehicleDetails: VehicleDetails?

    weak var vehicleDetailsViewControllerDelegate: VehicleDetailsViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let vehicleDetails = vehicleDetails {
            // data fetched faster then controller loads views
            vehicleCardView.show(vehicleDetails)
        } else {
            // fetching still ongoing
            vehicleCardView.startAnimating()
        }
    }

    @IBAction private func dismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        vehicleDetailsViewControllerDelegate?.dismissed(vehicleDetailsViewController: self)
    }

    @IBAction private func rent(_ sender: UIButton) {
        vehicleDetailsViewControllerDelegate?.rent(vehiclePresented: self)
    }

}

extension VehicleDetailsViewController: VehicleDetailsViewModelDelegate {

    func fetchCompleted(with details: VehicleDetails) {
        // to survive if data are fetched faster then controller loads views
        if isViewLoaded {
            vehicleCardView?.show(details)
            vehicleDetails = nil
        } else {
            vehicleDetails = details
        }
    }

    func fetchFailed(with reason: String) {
        vehicleDetails = nil
        vehicleCardView?.stopAnimating()
        print("\(#function) \(reason)") // TODO: !!!
    }

}
