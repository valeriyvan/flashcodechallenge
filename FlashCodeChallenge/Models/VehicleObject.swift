import Foundation
import MapKit

// MKAnnotation needs NSObject.
// This wrapper of Vehicle struct is less hassle then making Vehicle NSObject.

// Because task description says that vehicle details should be shown with
// request vehicles/<id>, vehicle property intensionally made private.
// Only its id is exposed with computed property and coordinate is exposed
// to conform to MKAnnotation protocol.

final class VehicleAnnotation: NSObject {
    private let vehicle: Vehicle

    var id: Int { return vehicle.id }

    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: vehicle.latitude,
                                      longitude: vehicle.longitude)
    }

    init(vehicle: Vehicle) {
        self.vehicle = vehicle
    }
}

extension VehicleAnnotation: MKAnnotation {}
