import Foundation

// Actually, everything but id, latitude, longitude might be commented out.
// The rest will be fetched with vehicle/<id> request according to task description.

struct Vehicle {
    let id: Int
    //let name: String
    //let description: String
    let latitude: Double
    let longitude: Double
    //let batteryLevel: Int
    //let timestamp: Date
    //let price: Int
    //let priceTime: Int
    //let currency: String // TODO: enum
}

extension Vehicle: Codable {}
