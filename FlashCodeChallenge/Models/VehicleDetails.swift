import Foundation

struct VehicleDetails {
    let id: Int
    let name: String
    let description: String
    let latitude: Double
    let longitude: Double
    let batteryLevel: Int
    let timestamp: Date
    let price: Int
    let priceTime: Int
    let currency: String // TODO: enum
}

extension VehicleDetails: Codable {}
