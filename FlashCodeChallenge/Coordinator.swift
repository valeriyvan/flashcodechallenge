protocol Coordinator {
    func start()
    func dismiss()
}
