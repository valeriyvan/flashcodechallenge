import Foundation

protocol VehiclesViewModelDelegate: class {
    func fetchCompleted(with: [Vehicle])
    func fetchFailed(with reason: String)
}

final class VehiclesViewModel {
    private weak var delegate: VehiclesViewModelDelegate?

    private var inProgress: Bool = false

    private let client: VehiclesApiProtocol

    init(delegate: VehiclesViewModelDelegate, apiClient: VehiclesApiProtocol = VehiclesApiClient()) {
        self.delegate = delegate
        self.client = apiClient
    }

    func fetch() {
        guard !inProgress else { return }
        inProgress = true
        client.fetchVehicles(parametriser: nil, queue: .main) { [weak self] result in
            guard let sSelf = self else { return }
            sSelf.inProgress = false
            switch result {
            case .failure(let error):
                sSelf.delegate?.fetchFailed(with: error.localizedDescription)
            case .success(let poiList):
                sSelf.delegate?.fetchCompleted(with: poiList)
            }
        }
    }

    func cancel() {
        client.cancel()
    }

}
