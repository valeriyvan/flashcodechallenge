import Foundation

protocol VehicleDetailsViewModelDelegate: class {
    func fetchCompleted(with: VehicleDetails)
    func fetchFailed(with reason: String)
}

final class VehicleDetailsViewModel {
    private weak var delegate: VehicleDetailsViewModelDelegate?

    private var inProgress: Bool = false

    let client: VehicleDetailsApiProtocol

    init(delegate: VehicleDetailsViewModelDelegate, apiClient: VehicleDetailsApiProtocol = VehicleDetailsApiClient()) {
        self.delegate = delegate
        self.client = apiClient
    }

    func fetch(for id: Int) {
        guard !inProgress else { return }
        let parametriser: (URL) -> URL = { $0.appendingPathComponent(String(id)) }
        client.fetchVehicleDetails(parametriser: parametriser, queue: .main) { [weak self] result in
            guard let sSelf = self else { return }
            sSelf.inProgress = false
            switch result {
            case .failure(let error):
                sSelf.delegate?.fetchFailed(with: error.localizedDescription)
            case .success(let poiList):
                sSelf.delegate?.fetchCompleted(with: poiList)
            }
        }
    }

    func cancel() {
        client.cancel()
    }

}
