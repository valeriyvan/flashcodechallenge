import UIKit

class AppCoordinator: Coordinator {
    private let window: UIWindow
    let mapCoordinator: MapCoordinator

    init(window: UIWindow) {
        self.window = window
        self.mapCoordinator = MapCoordinator(window: window)
    }

    func start() {
        mapCoordinator.start()
        window.makeKeyAndVisible()
    }

    func dismiss() {}
}
