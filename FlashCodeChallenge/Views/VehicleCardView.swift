import UIKit

final class VehicleCardView: UIView {

    @IBOutlet private weak var activitiIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var batteryLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var rentButton: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 10.0
    }

    func startAnimating() {
        titleLabel.text = " "
        batteryLabel.text = " "
        priceLabel.text = " "
        rentButton.setTitle("Rent".localized, for: .normal)
        rentButton.isEnabled = false
        activitiIndicatorView.startAnimating()
    }

    func stopAnimating() {
        activitiIndicatorView.stopAnimating()
    }

    func show(_ details: VehicleDetails) {
        titleLabel.text = "\(details.description) \(details.name) #\(details.id)"
        batteryLabel.text = "Battery".localized + " \(details.batteryLevel)%"
        priceLabel.text = "Price".localized + " \(details.price)\(details.currency) "
            + "per".localized + " " + "min".localized
        rentButton.isEnabled = true
        activitiIndicatorView.stopAnimating()
    }

}
