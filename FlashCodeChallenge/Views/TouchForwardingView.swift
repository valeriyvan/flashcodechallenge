import UIKit

// https://pspdfkit.com/blog/2015/presentation-controllers/

final class TouchForwardingView: UIView {
    
    final var passthroughViews: [UIView] = []
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let hitView = super.hitTest(point, with: event) else { return nil }
        guard hitView == self else { return hitView }
        return passthroughViews
            .lazy
            .map { [unowned self] in $0.hitTest(self.convert(point, to: $0), with: event) }
            .first { $0 != nil }
            ?? self
    }
}
