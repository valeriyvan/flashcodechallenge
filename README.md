TODO:
* ✔️~~Generalise `VehiclesApiClient.swift` and `VehicleDetailsApiClient.swift` to get rid of code duplication;~~
* Generalise `VehiclesViewModel.swift` and `VehicleDetailsViewModel.swift` to get rid of code duplication;
* Update `MapViewController.swift` to change map region after fetching vehicles;
* More tests;
* Alternative implementations of bindings with `PromiseKit` and `RxSwift`;
* ✔️~~Use Coordinator to orchestrate view controllers.~~
